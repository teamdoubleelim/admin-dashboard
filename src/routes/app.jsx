import Dashboard from 'views/Dashboard/Dashboard';
import Singles from 'views/Bracket/Singles';
import Doubles from 'views/Bracket/Doubles';


const appRoutes = [
    { path: "/dashboard", name: "Dashboard", icon: "pe-7s-graph", component: Dashboard },
    { path: "/singles", name: "Singles", icon: "pe-7s-users", component: Singles },
    { path: "/doubles", name: "Doubles", icon: "pe-7s-user", component: Doubles },

    { redirect: true, path:"/", to:"/dashboard", name: "Dashboard" }
];

export default appRoutes;
